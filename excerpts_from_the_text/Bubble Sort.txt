﻿Bubble Sort

The bubble sort is notoriously slow, but it’s conceptually the simplest of the sorting
algorithms and for that reason is a good beginning for our exploration of sorting
techniques.

Bubble Sort on the Baseball Players

Imagine that you’re near-sighted (like a computer program) so that you can see only
two of the baseball players at the same time, if they’re next to each other and if you
stand very close to them. Given this impediment, how would you sort them? Let’s
assume there are N players, and the positions they’re standing in are numbered from
0 on the left to N-1 on the right.
The bubble sort routine works like this: You start at the left end of the line and
compare the two kids in positions 0 and 1. If the one on the left (in 0) is taller, you
swap them. If the one on the right is taller, you don’t do anything. Then you move
over one position and compare the kids in positions 1 and 2. Again, if the one on
the left is taller, you swap them. This sorting process is shown in Figure 3.3.
Here are the rules you’re following:
1. Compare two players.
2. If the one on the left is taller, swap them.
3. Move one position right.
You continue down the line this way until you reach the right end. You have
by no means finished sorting the kids, but you do know that the tallest kid is
on the right. This must be true because, as soon as you encounter the tallest
kid, you’ll end up swapping him (or her) every time you compare two kids,
until eventually he (or she) will reach the right end of the line. This is why it’s
called the bubble sort: As the algorithm progresses, the biggest items “bubble
up” to the top end of the array. Figure 3.4 shows the baseball players at the end
of the first pass.
After this first pass through all the data, you’ve made N-1 comparisons and
somewhere between 0 and N-1 swaps, depending on the initial arrangement of
the players. The item at the end of the array is sorted and won’t be moved
again.
Now you go back and start another pass from the left end of the line. Again,
you go toward the right, comparing and swapping when appropriate. However,
this time you can stop one player short of the end of the line, at position N-2,
because you know the last position, at N-1, already contains the tallest player.
This rule could be stated as:
4. When you reach the first sorted player, start over at the left end of the line.
You continue this process until all the players are in order. Describing this process is
much harder than demonstrating it, so let’s watch the BubbleSort Workshop applet
at work.

Efficiency of the Bubble Sort


As you can see by watching the BubbleSort Workshop applet with 10 bars, the inner
and inner+1 arrows make nine comparisons on the first pass, eight on the second,
and so on, down to one comparison on the last pass. For 10 items, this is
9 + 8 + 7 + 6 + 5 + 4 + 3 + 2 + 1 = 45
In general, where N is the number of items in the array, there are N-1 comparisons
on the first pass, N-2 on the second, and so on. The formula for the sum of such a
series is
(N–1) + (N–2) + (N–3) + ... + 1 = N*(N–1)/2
N*(N–1)/2 is 45 (10*9/2) when N is 10.
Thus, the algorithm makes about N2⁄2 comparisons (ignoring the –1, which doesn’t
make much difference, especially if N is large).
There are fewer swaps than there are comparisons because two bars are swapped only
if they need to be. If the data is random, a swap is necessary about half the time, so
there will be about N2⁄4 swaps. (Although in the worst case, with the initial data
inversely sorted, a swap is necessary with every comparison.)
Both swaps and comparisons are proportional to N2. Because constants don’t count
in Big O notation, we can ignore the 2 and the 4 and say that the bubble sort runs in
O(N2) time. This is slow, as you can verify by running the BubbleSort Workshop
applet with 100 bars.
Whenever you see one loop nested within another, such as those in the bubble sort
and the other sorting algorithms in this chapter, you can suspect that an algorithm
runs in O(N2) time. The outer loop executes N times, and the inner loop executes N
(or perhaps N divided by some constant) times for each cycle of the outer loop. This
means you’re doing something approximately N*N or N2 times.