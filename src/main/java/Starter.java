import text_loading.TextLoader;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Starter {
    public static void main(String[] args) throws IOException {
        int randomLength = 7;
        List<Path> txtFiles = TextLoader.loadFiles();
        Path path = txtFiles.get(0);
        String[] sentences = readSentencesFromTheFile(path);
        Map<String, Integer> dictionary = createDictionary(sentences);
        if (sentences.length < 1){
            System.out.println("There are not any sentences");
            return;
        }
        Set<String> uniqueWordsInFirstSentence = findUniqueWordInFirstSentence_3(sentences[0], dictionary);
        sortSentencesByNumberOfWords_2(sentences, path);
        Set<String> wordsOfGivenLength = findWordsOfGivenLengthFromQuestions_4(sentences, randomLength);
        writeSortedWordsIntoFile_6(dictionary, path);
        writeSortedWordsByVowelsIntoFile_7(dictionary, path);
        writeSortedWordsStartingWithVowelIntoFile_8(dictionary, path);
        System.out.println("Unique words in first sentence");
        uniqueWordsInFirstSentence.forEach(w -> System.out.print(w + " "));
        System.out.println();
        System.out.println("Words of length 7");
        wordsOfGivenLength.forEach(w -> System.out.println(w + " "));
        System.out.println();
    }

    // I
    private static String[] readSentencesFromTheFile(Path path) throws IOException {
        return new String(Files.readAllBytes(path)).toLowerCase().split("(\\.+|\\?+|!+) ");
    }

    // II
    private static Map<String, Integer> createDictionary(String[] sentences){
        Map<String, Integer> dictionary = new TreeMap<>();
        Stream.of(sentences).flatMap((sentence) -> {
            String[] words = sentence.split("\\W");
            return Stream.of(words);
        }).forEach((word) -> dictionary.merge(word, 1, Integer::sum));
        return dictionary;
    }

    // IV
    private static void sortSentencesByNumberOfWords_2(String[] sentences, Path path) throws IOException {
        Arrays.sort(sentences, (s1, s2) ->{
            String[] arr1 = s1.split("\\W");
            String[] arr2 = s2.split("\\W");
            return arr1.length - arr2.length;
        });
        String template = "2_SentencesSortedByWordsNumber_";
        String pathName = path.toString();
        String fileName = pathName.substring(pathName.lastIndexOf("\\") + 1);
        Path targetFile = Paths.get("D:\\EPAM_studying\\task06_String\\result" + "\\" + template + fileName);
        Path p = Files.createFile(targetFile);
        try (BufferedWriter fileWriter = new BufferedWriter
                (new OutputStreamWriter(new FileOutputStream(p.toFile())))){
            for (String sentence : sentences) {
                fileWriter.write(sentence);
                fileWriter.newLine();
            }
        }
    }

    // III
    private static Set<String> findUniqueWordInFirstSentence_3(String _1stSentence, Map<String, Integer> dictionary){
        String[] words = _1stSentence.split("\\W");
        Map<String, Integer> wordsAppearance = new HashMap<>();
        Set<String> uniqueWords = new HashSet<>();
        Stream.of(words).forEach((word) -> wordsAppearance.merge(word, 1, Integer::sum));
        wordsAppearance.forEach((k, v) -> {
            if (dictionary.containsKey(k)){
                if (dictionary.get(k).equals(v)){
                    uniqueWords.add(k);
                }
            } else {
                uniqueWords.add(k);
            }
        });
        return uniqueWords;
    }

    private static Set<String> findWordsOfGivenLengthFromQuestions_4(String[] sentences, int length){
        final Pattern pattern = Pattern.compile(".+\\?");
        Matcher matcher;
        Set<String> wordsOfGivenLength = new HashSet<>();
        for (String sentence : sentences){
            matcher = pattern.matcher(sentence);
            if (matcher.find()){
                Stream.of(sentence).flatMap(s -> Stream.of(s.split("\\W")))
                        .filter(w -> w.length() == length)
                        .forEach(wordsOfGivenLength::add);
            }
        }
        return wordsOfGivenLength;
    }

    private static void writeSortedWordsIntoFile_6(Map<String, Integer> dictionary, Path path) throws IOException {
        String template = "6_WordsSortedByName_";
        String pathName = path.toString();
        String fileName = pathName.substring(pathName.lastIndexOf("\\") + 1);
        Path targetFile = Paths.get("D:\\EPAM_studying\\task06_String\\result" + "\\" + template + fileName);
        Path p = Files.createFile(targetFile);
        try (BufferedWriter fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(p.toFile())))){
            String ch = "a";
            for (Map.Entry<String, Integer> entry : dictionary.entrySet()){
                String word = entry.getKey();
                if (word.startsWith(ch)){
                    fileWriter.write(ch.toUpperCase());
                    fileWriter.write(' ');
                } else {
                    fileWriter.newLine();
                    ch = word.substring(0, 1);
                    fileWriter.write(ch.toUpperCase());
                    fileWriter.write(' ');
                }
                fileWriter.write(word);
                fileWriter.write(' ');
            }
        }
    }

    private static void writeSortedWordsByVowelsIntoFile_7(Map<String, Integer> dictionary, Path path) throws IOException {
        Comparator<String> comparator = (w1, w2)-> {
            Pattern pattern = Pattern.compile("([aeiuoy])");
            Matcher m1 = pattern.matcher(w1);
            Matcher m2 = pattern.matcher(w2);
            int i1 = m1.groupCount();
            int i2 = m2.groupCount();
            Double d1 = (double) i1 / w1.length();
            Double d2 = (double) i2 / w2.length();
            return d1.compareTo(d2);
        };
        Map<String, Integer> wordsSortedByVowels = new TreeMap<>(comparator);
        wordsSortedByVowels.putAll(dictionary);
        String template = "7_WordsSortedByVowels_";
        String pathName = path.toString();
        String fileName = pathName.substring(pathName.lastIndexOf("\\") + 1);
        Path targetFile = Paths.get("D:\\EPAM_studying\\task06_String\\result" + "\\" + template + fileName);
        Path p = Files.createFile(targetFile);
        try (BufferedWriter fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(p.toFile())))){
            for (Map.Entry<String, Integer> entry : wordsSortedByVowels.entrySet()){
                fileWriter.write(entry.getKey());
                fileWriter.newLine();
            }
        }
    }

    private static void writeSortedWordsStartingWithVowelIntoFile_8(Map<String, Integer> dictionary, Path path) throws IOException{
        Comparator<String> comparator = (w1, w2)-> {
            String temp1 = w1.replaceFirst("[^aeiou]", "#");
            String temp2 = w2.replaceFirst("[^aeiou]", "#");
            int i1 = temp1.indexOf("#");
            int i2 = temp2.indexOf("#");
            return w1.substring(i1).compareTo(w2.substring(i2));
        };
        Set<String> wordsStartingWithVowels = new TreeSet<>(comparator);
        Pattern pattern = Pattern.compile("^[aeiuo]");
        Matcher matcher;
        for (Map.Entry<String, Integer> entry : dictionary.entrySet()){
            String word = entry.getKey();
            matcher = pattern.matcher(word);
            if (matcher.find()){
                wordsStartingWithVowels.add(word);
            }
        }
        String template = "8_SortedWordsStartingWithVowelsByNextConsonant_";
        String pathName = path.toString();
        String fileName = pathName.substring(pathName.lastIndexOf("\\") + 1);
        Path targetFile = Paths.get("D:\\EPAM_studying\\task06_String\\result" + "\\" + template + fileName);
        Path p = Files.createFile(targetFile);
        try (BufferedWriter fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(p.toFile())))){
            for (String word : wordsStartingWithVowels){
                fileWriter.write(word);
                fileWriter.newLine();
            }
        }
    }
}
