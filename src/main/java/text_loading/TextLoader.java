package text_loading;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class TextLoader {
    public static List<Path> loadFiles() throws IOException {
        String root = "D:\\EPAM_studying\\task06_String\\excerpts_from_the_text";
        List<Path> txtFiles = new ArrayList<>();
        try(Stream<Path> pathStream = Files.walk(Paths.get(root))){
            pathStream.filter(Files::isRegularFile)
                    .filter((e) -> e.getFileName().toString().endsWith(".txt"))
                    .forEach(txtFiles::add);
        }
        return txtFiles;
    }
}
